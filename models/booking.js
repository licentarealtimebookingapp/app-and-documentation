const mongoose = require('mongoose');

const bookingSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: { type: String, required: true },
    client_name: { type: String, required: true },
    description: { type: String, required: true },
    seats: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Seat' }],
    user_id: { type: mongoose.Schema.ObjectId, ref: 'User' },
    start: { type: Date, required: true },
    end: { type: Date },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Booking', bookingSchema);