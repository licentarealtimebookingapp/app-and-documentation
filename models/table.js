const mongoose = require('mongoose');

const tableSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true },
    user_id: { type: mongoose.Schema.ObjectId, ref: 'User' },
    seats: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Seat' }],
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Table', tableSchema);