const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

mongoose.connect(
    "mongodb://rest-booking:" +
    process.env.MONGO_ATLAS_PASSWORD +
    "@node-real-time-booking-app-shard-00-00-6gbuu.mongodb.net:27017,node-real-time-booking-app-shard-00-01-6gbuu.mongodb.net:27017,node-real-time-booking-app-shard-00-02-6gbuu.mongodb.net:27017/test?ssl=true&replicaSet=node-real-time-booking-app-shard-0&authSource=admin"
);

mongoose.Promise = global.Promise;

const usersRoutes = require('./routes/users');
const productRoutes = require('./routes/products');
const tableRoutes = require('./routes/tables');
const seatRoutes = require('./routes/seats');
const orderRoutes = require('./routes/orders');
const bookingRoutes = require('./routes/bookings');

const app = express();

app.set('port', process.env.PORT || 3000);

app.use(cors());
app.use(morgan("dev"));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
app.use(bodyParser.json({limit: "50mb"}));

// API Routes
app.use('/users', usersRoutes);
app.use("/products", productRoutes);
app.use("/tables", tableRoutes);
app.use("/seats", seatRoutes);
app.use("/orders", orderRoutes);
app.use("/bookings", bookingRoutes);

app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;
