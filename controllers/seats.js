const mongoose = require("mongoose");
const Seat = require("../models/seat");

exports.seats_get_all = (req, res, next) => {
    Seat.aggregate([
        { $match: { user_id: mongoose.Types.ObjectId(req.userData["userId"]) } },
        {
            $lookup: {
                "from": "tables",
                "localField": "table_id",
                "foreignField": "_id",
                "as": "table"
            }
        }
    ]).exec()
        .then(docs => {
            const response = {
                data: docs.map(doc => {
                    return {
                        name: doc.name,
                        created_at: doc.created_at,
                        updated_at: doc.updated_at,
                        _id: doc._id,
                        user_id: doc.user_id,
                        table_id: doc.table_id,
                        table: doc.table,
                        occupied: doc.occupied,
                        request: {
                            type: "GET",
                            url: "http://localhost:3000/seats/" + doc._id
                        }
                    };
                })
            };
            //   if (docs.length >= 0) {
            res.status(200).json(response);
            //   } else {
            //       res.status(404).json({
            //           message: 'No entries found'
            //       });
            //   }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.seats_create_seat = (req, res, next) => {
    const seat = new Seat({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        table_id: req.body.table_id,
        user_id: req.userData["userId"]
    });
    seat
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: "Created seat successfully",
                createdTable: {
                    name: result.name,
                    table_id: result.table_id,
                    created_at: result.created_at,
                    updated_at: result.updated_at,
                    user_id: result.user_id,
                    occupied: result.occupied,
                    _id: result._id,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/seats/" + result._id
                    }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.seats_get_seat = (req, res, next) => {
    const id = req.params.seatId;
    Seat.find({ _id: id, user_id: req.userData["userId"] })
        .select("_id name occupied table_id created_at updated_at user_id")
        .exec()
        .then(doc => {
            console.log("From database", doc);
            if (doc) {
                res.status(200).json({
                    table: doc,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/seats"
                    }
                });
            } else {
                res
                    .status(404)
                    .json({ message: "No valid entry found for provided ID" });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
};

exports.seats_update_seat = (req, res, next) => {
    const id = req.params.seatId;
    Seat.findByIdAndUpdate(id, req.body)
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Seat updated",
                request: {
                    type: "GET",
                    url: "http://localhost:3000/seats/" + id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.seats_delete = (req, res, next) => {
    const id = req.params.seatId;
    Seat.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Seat deleted",
                request: {
                    type: "POST",
                    url: "http://localhost:3000/seats",
                    body: { name: "String" }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};