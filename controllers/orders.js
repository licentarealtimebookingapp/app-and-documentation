const mongoose = require("mongoose");
const Order = require("../models/order");
var PDFDocument = require('pdfkit');

exports.orders_get_all = (req, res, next) => {
    Order.aggregate([
        { $match: { user_id: mongoose.Types.ObjectId(req.userData["userId"]) } },
        {
            $lookup: {
                "from": "seats",
                "localField": "seats",
                "foreignField": "_id",
                "as": "seats"
            }
        },
        {
            $lookup: {
                "from": "products",
                "localField": "products",
                "foreignField": "_id",
                "as": "products"
            }
        }
    ]).exec()
        .then(docs => {
            const response = {
                data: docs.map(doc => {
                    return {
                        name: doc.name,
                        created_at: doc.created_at,
                        updated_at: doc.updated_at,
                        _id: doc._id,
                        user_id: doc.user_id,
                        seats: doc.seats,
                        products: doc.products,
                        total: doc.total,
                        request: {
                            type: "GET",
                            url: "http://localhost:3000/orders/" + doc._id
                        }
                    };
                })
            };
            //   if (docs.length >= 0) {
            res.status(200).json(response);
            //   } else {
            //       res.status(404).json({
            //           message: 'No entries found'
            //       });
            //   }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.orders_create_order = (req, res, next) => {
    const order = new Order({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        seats: req.body.seats,
        products: req.body.products,
        total: req.body.total,
        user_id: req.userData["userId"]
    });

    order
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: "Created order successfully",
                createdTable: {
                    name: result.name,
                    seats: result.seats,
                    products: result.products,
                    total: result.total,
                    created_at: result.created_at,
                    updated_at: result.updated_at,
                    user_id: result.user_id,
                    _id: result._id,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/orders/" + result._id
                    }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.orders_get_order = (req, res, next) => {
    const id = req.params.orderId;
    Order.find({ _id: id, user_id: req.userData["userId"] })
        .select("_id name seats products created_at updated_at user_id")
        .exec()
        .then(doc => {
            console.log("From database", doc);
            if (doc) {
                res.status(200).json({
                    table: doc,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/orders"
                    }
                });
            } else {
                res
                    .status(404)
                    .json({ message: "No valid entry found for provided ID" });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
};

exports.orders_export_order = (req, res, next) => {
    const id = req.params.orderId;
    const doc = new PDFDocument();

    Order.aggregate([
        { $match: { /*user_id: mongoose.Types.ObjectId(req.userData["userId"]), */_id: mongoose.Types.ObjectId(id) } },
        {
            $lookup: {
                "from": "seats",
                "localField": "seats",
                "foreignField": "_id",
                "as": "seats"
            }
        },
        {
            $lookup: {
                "from": "products",
                "localField": "products",
                "foreignField": "_id",
                "as": "products"
            }
        }
    ]).exec()
        .then(data => {
            console.log(data);
            let filename = encodeURIComponent(data[0].name) + '.pdf';
            res.setHeader('Content-disposition', 'attachment; filename="' + filename + '"');
            res.setHeader('Content-type', 'application/pdf');

            doc.font('Times-Roman', 18)
                .fontSize(25)
                .text('Order: ' + data[0].name, 100, 50);

            doc.fontSize(15)
                .fillColor('green')
                .text('Seats: ' + data[0].seats.map(function(value) {
                    return value.name;
                }), 100, 100);

            doc.fontSize(15)
                .fillColor('green')
                .text('Products: ' + data[0].products.map(function(value) {
                    return value.name;
                }), 100, 150);

            doc.moveDown()
                .fillColor('red')
                .text("Total: " + data[0].total);

            doc.pipe(res);

            doc.end();
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.orders_update_order = (req, res, next) => {
    const id = req.params.orderId;
    Order.findByIdAndUpdate(id, req.body)
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Order updated",
                request: {
                    type: "GET",
                    url: "http://localhost:3000/orders/" + id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.orders_delete = (req, res, next) => {
    const id = req.params.orderId;
    Order.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Order deleted",
                request: {
                    type: "POST",
                    url: "http://localhost:3000/orders",
                    body: { name: "String" }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};