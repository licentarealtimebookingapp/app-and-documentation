const mongoose = require("mongoose");
const Table = require("../models/table");
const Seat = require("../models/seat");

exports.tables_get_all = (req, res, next) => {
    Table.find({ user_id: req.userData["userId"] })
        .select("_id name seats created_at updated_at user_id")
        .exec()
        .then(docs => {
            const response = {
                data: docs.map(doc => {
                    return {
                        name: doc.name,
                        created_at: doc.created_at,
                        updated_at: doc.updated_at,
                        _id: doc._id,
                        user_id: doc.user_id,
                        seats: doc.seats,
                        request: {
                            type: "GET",
                            url: "http://localhost:3000/tables/" + doc._id
                        }
                    };
                })
            };
            //   if (docs.length >= 0) {
            res.status(200).json(response);
            //   } else {
            //       res.status(404).json({
            //           message: 'No entries found'
            //       });
            //   }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.tables_create_table = (req, res, next) => {
    const table = new Table({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        seats: req.body.seats,
        user_id: req.userData["userId"]
    });
    table
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: "Created table successfully",
                createdTable: {
                    name: result.name,
                    seats: result.seats,
                    created_at: result.created_at,
                    updated_at: result.updated_at,
                    user_id: result.user_id,
                    _id: result._id,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/tables/" + result._id
                    }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.tables_get_table = (req, res, next) => {
    const id = req.params.tableId;
    Table.find({ _id: id, user_id: req.userData["userId"] })
        .select("_id name seats created_at updated_at user_id")
        .exec()
        .then(doc => {
            console.log("From database", doc);
            if (doc) {
                res.status(200).json({
                    table: doc,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/tables"
                    }
                });
            } else {
                res
                    .status(404)
                    .json({ message: "No valid entry found for provided ID" });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
};

exports.tables_update_table = (req, res, next) => {
    const id = req.params.tableId;
    Table.findByIdAndUpdate(id, req.body)
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Table updated",
                request: {
                    type: "GET",
                    url: "http://localhost:3000/tables/" + id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.tables_delete = (req, res, next) => {
    const id = req.params.tableId;
    Table.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Table deleted",
                request: {
                    type: "POST",
                    url: "http://localhost:3000/tables",
                    body: { name: "String" }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};