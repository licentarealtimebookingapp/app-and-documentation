const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const User = require("../models/users");

exports.register = (req, res, next) => {
    User.find({email: req.body.email})
        .exec()
        .then(user => {
            if (user.length > 0) {
                return res.status(422).json({
                    message: "Email address already exists"
                });
            } else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            error: err
                        });
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            name: req.body.name,
                            description: req.body.description,
                            email: req.body.email,
                            password: hash
                        });
                        user
                            .save()
                            .then(result => {
                                const token = jwt.sign(
                                    {
                                        email: user.email,
                                        userId: user._id
                                    },
                                    process.env.JWT_SECRET,
                                    {
                                        expiresIn: "1h"
                                    }
                                );
                                res.status(201).json({
                                    message: "User created successfully",
                                    auth_token: token,
                                    expires_in: 3600
                                });
                            })
                            .catch(err => {
                                res.status(500).json({
                                    error: err
                                });
                            });
                    }
                });
            }
        });
};

exports.me = (req, res, next) => {
    User.findById(req.userData["userId"])
        .select("name description email _id")
        .exec()
        .then(user => {
            res.status(200).json({
                data: user
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
};

exports.login = (req, res, next) => {
    User.find({ email: req.body.email })
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(422).json({
                    message: "Invalid credentials"
                });
            }
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if (err) {
                    return res.status(422).json({
                        message: "Invalid credentials"
                    });
                }
                if (result) {
                    const token = jwt.sign(
                        {
                            email: user[0].email,
                            userId: user[0]._id
                        },
                        process.env.JWT_SECRET,
                        {
                            expiresIn: "1h"
                        }
                    );
                    return res.status(200).json({
                        message: "User logged in successfully",
                        auth_token: token,
                        expires_in: 3600
                    });
                }
                res.status(401).json({
                    message: "Incorrect email or password."
                });
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
};

exports.update = (req, res, next) => {
    var password = req.body.password;

    bcrypt.hash(password, 10, function(err, hash) {
        req.body.password = hash;

        User.findOneAndUpdate({ _id: req.userData["userId"] }, req.body)
            .exec()
            .then(result => {
                res.status(204).json({
                    message: 'Your account was updated successfully',
                });
            })
            .catch(err => {
                res.status(500).json({
                    error: err
                });
            });
    });
};