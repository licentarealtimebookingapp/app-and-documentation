const mongoose = require("mongoose");
const Booking = require("../models/booking");

exports.bookings_get_all = (req, res, next) => {
    Booking.aggregate([
        { $match: { user_id: mongoose.Types.ObjectId(req.userData["userId"]) } },
        {
            $lookup: {
                "from": "seats",
                "localField": "seats",
                "foreignField": "_id",
                "as": "seats"
            }
        }
    ]).exec()
        .then(docs => {
            const response = {
                data: docs.map(doc => {
                    return {
                        title: doc.title,
                        created_at: doc.created_at,
                        updated_at: doc.updated_at,
                        _id: doc._id,
                        user_id: doc.user_id,
                        seats: doc.seats,
                        start: doc.start,
                        end: doc.end,
                        client_name: doc.client_name,
                        description: doc.description,
                        request: {
                            type: "GET",
                            url: "http://localhost:3000/bookings/" + doc._id
                        }
                    };
                })
            };
            //   if (docs.length >= 0) {
            res.status(200).json(response);
            //   } else {
            //       res.status(404).json({
            //           message: 'No entries found'
            //       });
            //   }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.bookings_create_booking = (req, res, next) => {
    const booking = new Booking({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        description: req.body.description,
        seats: req.body.seats,
        client_name: req.body.client_name,
        start: req.body.start,
        end: req.body.end,
        user_id: req.userData["userId"]
    });

    booking
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: "Created booking successfully",
                createdTable: {
                    title: result.title,
                    description: result.description,
                    seats: result.seats,
                    start: result.start,
                    end: result.end,
                    client_name: result.client_name,
                    created_at: result.created_at,
                    updated_at: result.updated_at,
                    user_id: result.user_id,
                    _id: result._id,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/bookings/" + result._id
                    }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.bookings_get_booking = (req, res, next) => {
    const id = req.params.bookingId;
    Booking.find({ _id: id, user_id: req.userData["userId"] })
        .select("_id title description seats client_name start end created_at updated_at user_id")
        .exec()
        .then(doc => {
            console.log("From database", doc);
            if (doc) {
                res.status(200).json({
                    table: doc,
                    request: {
                        type: "GET",
                        url: "http://localhost:3000/bookings"
                    }
                });
            } else {
                res
                    .status(404)
                    .json({ message: "No valid entry found for provided ID" });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({ error: err });
        });
};

exports.bookings_update_booking = (req, res, next) => {
    const id = req.params.bookingId;
    Booking.findByIdAndUpdate(id, req.body)
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Booking updated",
                request: {
                    type: "GET",
                    url: "http://localhost:3000/bookings/" + id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.bookings_delete = (req, res, next) => {
    const id = req.params.bookingId;
    Booking.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "Booking deleted",
                request: {
                    type: "POST",
                    url: "http://localhost:3000/bookings",
                    body: { title: "String" }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};