import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import {SeatService} from "../seat.service";
import {ISeat} from "../seat";
import {SeatAddComponent} from "../modals/seats/add/add.component";
import {SeatEditComponent} from "../modals/seats/edit/edit.component";
import {SeatDeleteComponent} from "../modals/seats/delete/delete.component";

@Component({
  selector: 'app-seats',
  templateUrl: './seats.component.html',
  styleUrls: ['./seats.component.css']
})
export class SeatsComponent implements AfterViewInit, OnInit {

  displayedColumns = ['name', 'table', 'actions'];
  dataSource = new MatTableDataSource();
  index: number;
  id: number;
  loaded = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  /**
   * Set the paginator after the view init since this component will
   * be able to query its view for the initialized paginator.
   */
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  rowClicked(row: any): void {
    //console.log(row);
  }

  constructor(private seatService: SeatService, public dialog: MatDialog) {

  }

  ngOnInit() {
    this.getData();
  }

  refresh() {
    this.getData();
  }

  getData() {
    this.seatService.getSeats()
      .subscribe(seats => {
        console.log(seats);
        this.dataSource.data = seats['data'];
        this.loaded = true;
      });
  }

  addItem(seat: ISeat) {
    const dialogRef = this.dialog.open(SeatAddComponent, {
      data: { seat: seat }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  editItem(i: number, id: number, name: string, table_id: string) {
    this.id = id;
    this.index = i;
    const dialogRef = this.dialog.open(SeatEditComponent, {
      data: {_id: id, name: name, table_id: table_id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  deleteItem(i: number, id: number, name: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(SeatDeleteComponent, {
      data: {id: id, name: name}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  /*private refreshTable() {
    // if there's a paginator active we're using it for refresh
    if (this.dataSource.paginator.hasNextPage()) {
      this.dataSource.paginator.nextPage();
      this.dataSource.paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource.paginator.hasPreviousPage()) {
      this.dataSource.paginator.previousPage();
      this.dataSource.paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
    }
  }*/

}
