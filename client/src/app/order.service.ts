import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IOrder} from "./order";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  domain: string = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  getOrders() {
    return this.http.get<IOrder[]>(`${this.domain}/orders`);
  }

  getOrder(id) {
    return this.http.get<IOrder>(`${this.domain}/orders/${id}`);
  }

  getOrderPdf(id) {
    return this.http.get(`${this.domain}/orders/${id}/pdf`);
  }

  addOrder(newOrder: IOrder) {
    return this.http.post<IOrder>(`${this.domain}/orders`, newOrder);
  }

  deleteOrder(id) {
    return this.http.delete<IOrder>(`${this.domain}/orders/${id}`);
  }

  updateOrder(order) {
    return this.http.patch<IOrder>(`${this.domain}/orders/${order._id}`, order);
  }
}
