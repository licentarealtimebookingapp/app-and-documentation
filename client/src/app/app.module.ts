import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatButtonModule,
  MatCardModule,
  MatToolbarModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatIconModule, MatDialogModule, MatSelectModule, MatOptionModule
} from "@angular/material";

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RegisterComponent } from './register/register.component';

import { AuthService } from './auth.service';
import { AuthInterceptor } from "./auth-interceptor";
import { LoginComponent } from './login/login.component';

import { AuthGuardService as AuthGuard } from './auth-guard.service';
import { ProfileComponent } from './profile/profile.component';
import { ProductsComponent } from './products/products.component';
import { AddComponent } from './modals/products/add/add.component';
import { EditComponent } from './modals/products/edit/edit.component';
import { DeleteComponent } from './modals/products/delete/delete.component';
import { ToastrModule } from 'ngx-toastr';
import {ProfileLayoutComponent} from "./layouts/profile-layout.component";
import {LoginLayoutComponent} from "./layouts/login-layout.component";
import {RegisterLayoutComponent} from "./layouts/register-layout.component";
import {ProductLayoutComponent} from "./layouts/product-layout.component";
import { DashboardComponent } from './dashboard/dashboard.component';
import {DashboardLayoutComponent} from "./layouts/dashboard-layout.component";
import { TablesComponent } from './tables/tables.component';
import {TablesLayoutComponent} from "./layouts/tables-layout.component";

import { TableAddComponent } from './modals/tables/add/add.component';
import {TableEditComponent} from "./modals/tables/edit/edit.component";
import {TableDeleteComponent} from "./modals/tables/delete/delete.component";
import { SeatsComponent } from './seats/seats.component';
import {SeatsLayoutComponent} from "./layouts/seats-layout.component";
import {SeatAddComponent} from "./modals/seats/add/add.component";
import {SeatEditComponent} from "./modals/seats/edit/edit.component";
import {SeatDeleteComponent} from "./modals/seats/delete/delete.component";
import {OrdersLayoutComponent} from "./layouts/orders-layout.component";
import { OrdersComponent } from './orders/orders.component';
import {OrderAddComponent} from "./modals/orders/add/add.component";
import {OrderEditComponent} from "./modals/orders/edit/edit.component";
import {OrderDeleteComponent} from "./modals/orders/delete/delete.component";
import { BookingsComponent } from './bookings/bookings.component';
import {BookingsLayoutComponent} from "./layouts/bookings-layout.component";
import {FullCalendarModule} from "ng-fullcalendar";
import {BookingAddComponent} from "./modals/bookings/add/add.component";

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {BookingDeleteComponent} from "./modals/bookings/delete/delete.component";
import {BookingEditComponent} from "./modals/bookings/edit/edit.component";
import { LandingComponent } from './landing/landing.component';
import {LandingLayoutComponent} from "./layouts/landing-layout.component";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    ProductsComponent,
    AddComponent,
    TableAddComponent,
    TableEditComponent,
    TableDeleteComponent,
    OrderAddComponent,
    OrderEditComponent,
    OrderDeleteComponent,
    EditComponent,
    DeleteComponent,
    ProfileLayoutComponent,
    LoginLayoutComponent,
    ProductLayoutComponent,
    RegisterLayoutComponent,
    DashboardLayoutComponent,
    OrdersLayoutComponent,
    BookingsLayoutComponent,
    DashboardComponent,
    TablesComponent,
    TablesLayoutComponent,
    SeatsComponent,
    SeatsLayoutComponent,
    SeatAddComponent,
    SeatEditComponent,
    SeatDeleteComponent,
    OrdersComponent,
    BookingsComponent,
    BookingAddComponent,
    BookingDeleteComponent,
    BookingEditComponent,
    LandingComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FullCalendarModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatButtonModule, MatCardModule, MatToolbarModule, MatInputModule, MatSelectModule, MatOptionModule, MatTableModule, MatPaginatorModule, MatSortModule, MatProgressSpinnerModule, MatIconModule, MatDialogModule,
    RouterModule.forRoot([
      {path: "register", component: RegisterComponent},
      /*{path: "login", component: LoginComponent},
      {path: "profile", component: ProfileComponent, canActivate: [AuthGuard]},*/

      {
        path: 'landing',
        //component: LandingLayoutComponent,
        children: [
          {
            path: '',
            component: LandingComponent
          }
        ]
      },
      {
        path: '',
        component: DashboardLayoutComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            component: DashboardComponent
          }
        ]
      },
      {
        path: 'profile',
        component: ProfileLayoutComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            component: ProfileComponent
          }
        ]
      },
      {
        path: 'bookings',
        component: BookingsLayoutComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            component: BookingsComponent
          }
        ]
      },
      {
        path: 'products',
        component: ProductLayoutComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            component: ProductsComponent
          }
        ]
      },
      {
        path: 'tables',
        component: TablesLayoutComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            component: TablesComponent
          }
        ]
      },
      {
        path: 'seats',
        component: SeatsLayoutComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            component: SeatsComponent
          }
        ]
      },
      {
        path: 'orders',
        component: OrdersLayoutComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            component: OrdersComponent
          }
        ]
      },
      {
        path: '',
        component: LoginLayoutComponent,
        children: [
          {
            path: 'login',
            component: LoginComponent
          }
        ]
      },
      {
        path: 'register',
        component: RegisterLayoutComponent,
        children: [
          {
            path: 'register',
            component: RegisterComponent
          }
        ]
      },
      { path: '**', redirectTo: '' }

    ])
  ],
  providers: [
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    { provide: FormBuilder, useValue: {} }
  ],
  entryComponents: [
    AddComponent,
    EditComponent,
    DeleteComponent,
    TableAddComponent,
    TableEditComponent,
    TableDeleteComponent,
    SeatAddComponent,
    SeatEditComponent,
    SeatDeleteComponent,
    OrderAddComponent,
    OrderEditComponent,
    OrderDeleteComponent,
    BookingAddComponent,
    BookingDeleteComponent,
    BookingEditComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
