import {Component, AfterViewInit, ViewChild, OnInit} from '@angular/core';
import {IProduct} from "../product";
import {ProductService} from "../product.service";
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import {DeleteComponent} from "../modals/products/delete/delete.component";
import {AddComponent} from "../modals/products/add/add.component";
import {EditComponent} from "../modals/products/edit/edit.component";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements AfterViewInit, OnInit {

  displayedColumns = ['image', 'name', 'price', 'actions'];
  dataSource = new MatTableDataSource();
  index: number;
  id: number;
  loaded = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  /**
   * Set the paginator after the view init since this component will
   * be able to query its view for the initialized paginator.
   */
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  constructor(private productService: ProductService, public dialog: MatDialog) {

  }

  ngOnInit() {
    this.getData();
  }

  refresh() {
    this.getData();
  }

  getData() {
    this.productService.getProducts()
      .subscribe(products => {
        this.dataSource.data = products['data'];
        this.loaded = true;
      });
  }

  addItem(product: IProduct) {
    const dialogRef = this.dialog.open(AddComponent, {
      data: { product: product }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  editItem(i: number, id: number, name: string, price: string, image: string) {
    this.id = id;
    this.index = i;
    const dialogRef = this.dialog.open(EditComponent, {
      data: {_id: id, name: name, price: price, image: image}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  deleteItem(i: number, id: number, name: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(DeleteComponent, {
      data: {id: id, name: name}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }
}

