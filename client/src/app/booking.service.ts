import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IBooking} from "./booking";

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  domain: string = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  getBookings() {
    return this.http.get<IBooking[]>(`${this.domain}/bookings`);
  }

  getBooking(id) {
    return this.http.get<IBooking>(`${this.domain}/bookings/${id}`);
  }

  addBooking(newBooking: IBooking) {
    return this.http.post<IBooking>(`${this.domain}/bookings`, newBooking);
  }

  deleteBooking(id) {
    return this.http.delete<IBooking>(`${this.domain}/bookings/${id}`);
  }

  updateBooking(booking) {
    return this.http.patch<IBooking>(`${this.domain}/bookings/${booking._id}`, booking);
  }
}
