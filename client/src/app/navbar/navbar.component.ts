import { Component, OnInit } from '@angular/core';
import { AuthService } from "../auth.service";
import {IUser} from "../user";
import {Observable} from "rxjs/index";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  private isLoggedIn;
  user: Observable<IUser>;

  constructor(private authService: AuthService) { this.isLoggedIn = this.authService.isLoggedIn(); }

  ngOnInit() {
    this.authService.me().subscribe((user: IUser) => {
      this.user = user['data'];
    });
  }

  logout() {
    this.authService.logout();
  }
}
