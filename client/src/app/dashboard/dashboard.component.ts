import { Component, OnInit } from '@angular/core';
import {SeatService} from "../seat.service";
import {ISeat} from "../seat";
import {OrderService} from "../order.service";
import {NotificationService} from "../services/notification.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  dbSeats: ISeat[];
  processedSeats = {};
  keys = [];
  loaded = false;
  ordersCount = 0;
  capacity = 0;
  ordersTotalInvoiced = 0;
  constructor(public seatService: SeatService, public orderService: OrderService,
              public notificationService: NotificationService) {

  }

  ngOnInit() {
    this.getCardsData();
    this.getData();
  }

  refresh() {
    this.getCardsData();
    this.getData();
  }

  getCardsData() {
    this.orderService.getOrders().subscribe(orders => {
      this.ordersCount = orders['data'].length;

      var totalInvoiced = 0;
      orders['data'].forEach(function(value) {
        let total = 0;
        if (!isNaN(value['total'])) {
          total = value['total'];
        }
        totalInvoiced += total;
      });

      this.ordersTotalInvoiced = totalInvoiced;
    });
  }

  getData() {
    this.seatService.getSeats()
      .subscribe(seats => {
        this.processedSeats = ['test'];
        this.dbSeats = seats['data'];

        this.capacity = this.dbSeats.length;

        let seatsData = {};
        this.dbSeats.forEach(function (value) {
          if(value['table_id']) {
              if(value['table'][0]['name'] in seatsData) {
                seatsData[value['table'][0]['name']].splice(0, 0, value);
              } else {
                seatsData[value['table'][0]['name']] = [value];
              }
          }
        });

        this.processedSeats = seatsData;
        this.keys = Object.keys(this.processedSeats);
        this.loaded = true;
      });
  }

  changeStatus(id, occupied) {
    this.seatService.updateSeat({ _id: id, occupied: occupied }).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
        this.getData();
      }
    });
  }
}
