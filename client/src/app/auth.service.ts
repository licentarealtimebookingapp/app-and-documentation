import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import {Router} from "@angular/router";
import {IUser} from "./user";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public errorMsg;
  public hasError;

  constructor(private http: HttpClient, public router: Router) { }

  register(registerData) {
    return this.http.post('http://localhost:3000/users/register', registerData)
      .subscribe(res => { this.setSession(res) },
        err => {
          this.hasError = true;
          this.errorMsg = err['error']['message'];
        });
  }

  me() {
    return this.http.get<IUser>('http://localhost:3000/users/me');
  }

  update(profileData) {
    return this.http.patch('http://localhost:3000/users/me', profileData)
      .subscribe(res => {
        console.log(res);
      });
  }

  login(loginData) {
    return this.http.post('http://localhost:3000/users/login', loginData)
      .subscribe(res => {
        this.setSession(res);
      },
      err => {
        this.hasError = true;
        this.errorMsg = err['error']['message'];
      });
  }

  private setSession(authResult) {
    const expiresAt = moment().add(authResult.expires_in,'second');

    localStorage.setItem('auth_token', authResult.auth_token);
    localStorage.setItem("expires_in", JSON.stringify(expiresAt.valueOf()) );

    this.router.navigate(['']);
  }

  logout() {
    localStorage.removeItem("auth_token");
    localStorage.removeItem("expires_in");

    this.router.navigate(['login']);
  }

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = localStorage.getItem("expires_in");
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }
}
