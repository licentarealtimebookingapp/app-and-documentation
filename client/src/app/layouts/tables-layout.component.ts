import { Component } from '@angular/core';

@Component({
  selector: 'app-tables-layout',
  template: `
    <app-navbar></app-navbar>
    <div class="main">
        <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class TablesLayoutComponent {}
