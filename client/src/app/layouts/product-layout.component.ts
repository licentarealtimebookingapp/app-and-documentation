import { Component } from '@angular/core';

@Component({
  selector: 'app-product-layout',
  template: `
    <app-navbar></app-navbar>
    <div class="main">
        <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class ProductLayoutComponent {}
