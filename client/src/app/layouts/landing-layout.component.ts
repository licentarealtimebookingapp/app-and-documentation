import { Component } from '@angular/core';

@Component({
  selector: 'app-landing-layout',
  template: `    
    <div class="main">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class LandingLayoutComponent {}
