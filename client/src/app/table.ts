export interface ITable {
  _id: string;
  name: string;
  seats: Array<Object>;
  user_id: string;
  created_at: string;
  updated_at: string;
}
