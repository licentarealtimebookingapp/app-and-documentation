import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IProduct} from "./product";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  domain: string = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  getProducts() {
    return this.http.get<IProduct[]>(`${this.domain}/products`);
  }

  getProduct(id) {
    return this.http.get<IProduct>(`${this.domain}/products/${id}`);
  }

  addProduct(newProduct: IProduct) {
    return this.http.post<IProduct>(`${this.domain}/products`, newProduct);
  }

  deleteProduct(id) {
    return this.http.delete<IProduct>(`${this.domain}/products/${id}`);
  }

  updateProduct(product) {
    return this.http.patch<IProduct>(`${this.domain}/products/${product._id}`, product);
  }
}
