import { Component, OnInit, ViewChild } from '@angular/core';
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';
import {BookingService} from "../booking.service";
import {IBooking} from "../booking";
import {BookingAddComponent} from "../modals/bookings/add/add.component";
import {MatDialog} from "@angular/material";
import {NotificationService} from "../services/notification.service";
import * as moment from 'moment/moment';
import {BookingDeleteComponent} from "../modals/bookings/delete/delete.component";
import {BookingEditComponent} from "../modals/bookings/edit/edit.component";

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {

  events = [];
  calendarOptions: Options;
  @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;
  constructor(public bookingService: BookingService, public dialog: MatDialog, public notificationService: NotificationService) { }

  ngOnInit() {
    this.bookingService.getBookings().subscribe(data => {
      this.calendarOptions = {
        editable: true,
        eventLimit: false,
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listMonth'
        },
        selectable: true,
        events: [],
      };
    });

    this.loadEvents();
  }

  clearEvents() {
    this.events = [];
  }
  loadEvents() {
    this.bookingService.getBookings().subscribe(data => {
      this.events = data['data'];
    });
  }

  refresh() {
    this.loadEvents();
  }

  addItem(booking: IBooking) {
    const dialogRef = this.dialog.open(BookingAddComponent, {
      data: { booking: booking }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  clickButton(model: any) {

  }

  eventClick(model: any) {
    this.editItem(model.event._id, model.event.title, model.event.seats, model.event.client_name, model.event.description, model.event.start, model.event.end);
  }

  editItem(id: number, title: string, seats: Array<Object>, client_name: string, description: string, start: Date, end: Date) {
    var newSeats = [];
    seats.forEach(function (value) {
      newSeats.push(value['_id']);
    });

    const dialogRef = this.dialog.open(BookingEditComponent, {
      data: {_id: id, title: title, seats: newSeats, client_name: client_name, description: description, start: start, end: end}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  updateEvent(model: any) {
    var event = {
      _id: model.event._id,
      start: model.event.start,
      end: model.event.end
    };

    this.bookingService.updateBooking(event).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }

      console.log(res);
    });
  }

  eventRender(model: any) {
    model.element.find(".fc-title").remove();
    model.element.find(".fc-time").remove();
    var new_description =
      moment(model.event.start).format("HH:mm") + '<br/>'
      + '<strong>Title: </strong><br/>' + model.event.title + '<br/>'
      + '<strong>Client Name: </strong><br/>' + model.event.client_name + '<br/>'
      + '<strong>Description: </strong><br/>' + model.event.description + '<br/>'
    ;

    if(model.event.seats.length > 0) {
      new_description += '<strong>Seats: </strong><br/>';

      model.event.seats.forEach(function (value) {
        new_description += value['name'] + '<br/>';
      });
    }
    model.element.append(new_description);
  }
}
