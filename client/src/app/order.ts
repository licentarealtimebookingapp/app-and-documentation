export interface IOrder {
  _id: string;
  name: string;
  products: Array<Object>;
  seats: Array<Object>;
  user_id: string;
  created_at: string;
  updated_at: string;
  total: number;
}
