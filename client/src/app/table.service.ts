import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ITable} from "./table";

@Injectable({
  providedIn: 'root'
})
export class TableService {

  domain: string = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  getTables() {
    return this.http.get<ITable[]>(`${this.domain}/tables`);
  }

  getTable(id) {
    return this.http.get<ITable>(`${this.domain}/tables/${id}`);
  }

  addTable(newTable: ITable) {
    return this.http.post<ITable>(`${this.domain}/tables`, newTable);
  }

  deleteTable(id) {
    return this.http.delete<ITable>(`${this.domain}/tables/${id}`);
  }

  updateTable(table) {
    return this.http.patch<ITable>(`${this.domain}/tables/${table._id}`, table);
  }
}
