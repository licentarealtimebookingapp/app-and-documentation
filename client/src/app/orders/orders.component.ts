import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource, MatSort} from "@angular/material";
import {OrderService} from "../order.service";
import {IOrder} from "../order";
import {OrderAddComponent} from "../modals/orders/add/add.component";
import {OrderEditComponent} from "../modals/orders/edit/edit.component";
import {OrderDeleteComponent} from "../modals/orders/delete/delete.component";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements AfterViewInit, OnInit {

  displayedColumns = ['name', 'seats', 'products', 'total', 'actions'];
  dataSource = new MatTableDataSource();
  index: number;
  id: number;
  loaded = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  /**
   * Set the paginator after the view init since this component will
   * be able to query its view for the initialized paginator.
   */
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  rowClicked(row: any): void {
    //console.log(row);
  }

  constructor(private orderService: OrderService, public dialog: MatDialog) {

  }

  ngOnInit() {
    this.getData();
  }

  refresh() {
    this.getData();
  }

  getData() {
    this.orderService.getOrders()
      .subscribe(orders => {
        console.log(orders);
        this.dataSource.data = orders['data'];
        this.loaded = true;
      });
  }

  addItem(order: IOrder) {
    const dialogRef = this.dialog.open(OrderAddComponent, {
      data: { order: order }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  exportItem(id: number) {
    window.location.href='http://localhost:3000/orders/' + id + '/pdf';
  }

  editItem(i: number, id: number, name: string, seats: Array<Object>, products: Array<Object>) {
    this.id = id;
    this.index = i;

    var newSeats = [], newProducts = [];
    seats.forEach(function (value) {
      newSeats.push(value['_id']);
    });

    var total = 0;
    products.forEach(function (value) {
      newProducts.push(value['_id']);
      total += value['price'];
    });

    const dialogRef = this.dialog.open(OrderEditComponent, {
      data: {_id: id, name: name, seats: newSeats, products: newProducts, total: total}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  deleteItem(i: number, id: number, name: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(OrderDeleteComponent, {
      data: {id: id, name: name}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  /*private refreshTable() {
    // if there's a paginator active we're using it for refresh
    if (this.dataSource.paginator.hasNextPage()) {
      this.dataSource.paginator.nextPage();
      this.dataSource.paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource.paginator.hasPreviousPage()) {
      this.dataSource.paginator.previousPage();
      this.dataSource.paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
    }
  }*/

}

