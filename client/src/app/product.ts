export interface IProduct {
  _id: string;
  name: string;
  price: number;
  image: string;
  user_id: string;
}
