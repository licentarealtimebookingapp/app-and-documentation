export interface ISeat {
  _id: string;
  name: string;
  occupied: boolean;
  table_id: string;
  table: Object;
  user_id: string;
  created_at: string;
  updated_at: string;
}
