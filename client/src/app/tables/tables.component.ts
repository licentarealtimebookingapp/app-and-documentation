import {Component, AfterViewInit, ViewChild, OnInit} from '@angular/core';
import {ITable} from "../table";
import {TableService} from "../table.service";
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import {TableDeleteComponent} from "../modals/tables/delete/delete.component";
import {TableAddComponent} from "../modals/tables/add/add.component";
import {TableEditComponent} from "../modals/tables/edit/edit.component";

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})

export class TablesComponent implements AfterViewInit, OnInit {

  displayedColumns = ['name', 'actions'];
  dataSource = new MatTableDataSource();
  index: number;
  id: number;
  loaded = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  /**
   * Set the paginator after the view init since this component will
   * be able to query its view for the initialized paginator.
   */
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  rowClicked(row: any): void {
    //console.log(row);
  }

  constructor(private tableService: TableService, public dialog: MatDialog) {

  }

  ngOnInit() {
    this.getData();
  }

  refresh() {
    this.getData();
  }

  getData() {
    this.tableService.getTables()
      .subscribe(tables => {
        console.log(tables);
        this.dataSource.data = tables['data'];
        this.loaded = true;
      });
  }

  addItem(table: ITable) {
    const dialogRef = this.dialog.open(TableAddComponent, {
      data: { table: table }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  editItem(i: number, id: number, name: string) {
    this.id = id;
    this.index = i;
    const dialogRef = this.dialog.open(TableEditComponent, {
      data: {_id: id, name: name}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  deleteItem(i: number, id: number, name: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(TableDeleteComponent, {
      data: {id: id, name: name}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  /*private refreshTable() {
    // if there's a paginator active we're using it for refresh
    if (this.dataSource.paginator.hasNextPage()) {
      this.dataSource.paginator.nextPage();
      this.dataSource.paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource.paginator.hasPreviousPage()) {
      this.dataSource.paginator.previousPage();
      this.dataSource.paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
    }
  }*/

}

