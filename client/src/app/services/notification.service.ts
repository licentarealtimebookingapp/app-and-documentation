import { Injectable } from '@angular/core';
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastr: ToastrService) {}

  showSuccess(message) {
    this.toastr.success(message, 'Success!');
  }

  showError(message) {
    this.toastr.error(message, 'Oops!');
  }

  showWarning(message) {
    this.toastr.warning(message, 'Alert!');
  }

  showInfo(message) {
    this.toastr.info(message);
  }
}
