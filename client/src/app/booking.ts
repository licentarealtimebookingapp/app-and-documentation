export interface IBooking {
  _id: string;
  title: string;
  description: string;
  client_name: string;
  seats: Array<Object>;
  user_id: string;
  start: Date;
  end: Date;
  created_at: string;
  updated_at: string;
}
