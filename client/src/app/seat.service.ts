import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ISeat} from "./seat";

@Injectable({
  providedIn: 'root'
})
export class SeatService {

  domain: string = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  getSeats() {
    return this.http.get<ISeat[]>(`${this.domain}/seats`);
  }

  getSeat(id) {
    return this.http.get<ISeat>(`${this.domain}/seats/${id}`);
  }

  addSeat(newSeat: ISeat) {
    return this.http.post<ISeat>(`${this.domain}/seats`, newSeat);
  }

  deleteSeat(id) {
    return this.http.delete<ISeat>(`${this.domain}/seats/${id}`);
  }

  updateSeat(seat) {
    return this.http.patch<ISeat>(`${this.domain}/seats/${seat._id}`, seat);
  }
}
