import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import {IUser} from "../user";
import {AuthService} from "../auth.service";

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {

  profileForm: FormGroup;
  user: Observable<IUser>;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.profileForm = new FormGroup ({
      name: new FormControl('', {
        validators: Validators.required
      }),
      description: new FormControl('', {
        validators: Validators.required
      }),
      email: new FormControl('', {
        validators: [
          Validators.required,
          Validators.pattern("[^ @]*@[^ @]*")
        ]
      }),
      password: new FormControl('', {
        validators: [
          Validators.required,
          Validators.minLength(6)
        ]
      })
    });

    this.user = this.authService.me()
      .pipe(
        tap(user => this.profileForm.patchValue(user['data'])),
      );
  }

  onSubmit(formData: any) {
    const userData = this.mapDateData(formData.value);

    this.authService.update(userData);
  }

  mapDateData(user: IUser): IUser {
    return user;
  }

}
