import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from "../../../services/notification.service";
import {SeatService} from "../../../seat.service";
import {ISeat} from "../../../seat";
import {IBooking} from "../../../booking";
import {BookingService} from "../../../booking.service";
import {BookingDeleteComponent} from "../delete/delete.component";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class BookingEditComponent implements OnInit {

  bookingForm: FormGroup;
  seats: ISeat[];

  constructor(public dialogRef: MatDialogRef<BookingEditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: IBooking,
              public seatService: SeatService, private notificationService: NotificationService, public bookingService: BookingService) { }


  submit() {

  }

  ngOnInit() {
    this.bookingForm = new FormGroup ({
      _id: new FormControl(this.data._id, {
      }),
      title: new FormControl(this.data.title, {
        validators: Validators.required
      }),
      seats: new FormControl(this.data.seats, {
        validators: Validators.required
      }),
      client_name: new FormControl(this.data.client_name, {
        validators: Validators.required
      }),
      description: new FormControl(this.data.description, {
        validators: Validators.required
      }),
      start: new FormControl(this.data.start['_i'], {
        validators: Validators.required
      }),
      end: new FormControl('', {

      }),
    });

    this.seatService.getSeats()
      .subscribe(seats => {
        console.log(seats);
        this.seats = seats['data'];
      });
  }

  public confirmEdit(formData: any): void {
    const bookingData = this.mapDateData(formData.value);

    this.bookingService.updateBooking(bookingData).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }

      console.log(res);
    });
  }

  mapDateData(booking: IBooking): IBooking {
    return booking;
  }
}
