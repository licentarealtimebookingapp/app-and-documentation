import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {NotificationService} from "../../../services/notification.service";
import {BookingService} from "../../../booking.service";

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class BookingDeleteComponent {

  constructor(public dialogRef: MatDialogRef<BookingDeleteComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public bookingService: BookingService, private notificationService: NotificationService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.bookingService.deleteBooking(this.data.id).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }
    });
  }

}
