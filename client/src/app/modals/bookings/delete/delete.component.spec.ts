import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableDeleteComponent } from './delete.component';

describe('TableDeleteComponent', () => {
  let component: TableDeleteComponent;
  let fixture: ComponentFixture<TableDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
