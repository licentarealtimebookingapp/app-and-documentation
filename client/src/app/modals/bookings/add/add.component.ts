import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import {NotificationService} from "../../../services/notification.service";
import {ISeat} from "../../../seat";
import {SeatService} from "../../../seat.service";
import {IProduct} from "../../../product";
import {BookingService} from "../../../booking.service";
import {IBooking} from "../../../booking";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class BookingAddComponent implements OnInit {

  bookingForm: FormGroup;
  seats: ISeat[];

  constructor(public dialogRef: MatDialogRef<BookingAddComponent>,
              @Inject(MAT_DIALOG_DATA) public data: IBooking,
              public bookingService: BookingService, public seatService: SeatService, private notificationService: NotificationService) { }


  ngOnInit() {
    this.bookingForm = new FormGroup ({
      title: new FormControl('', {
        validators: Validators.required
      }),
      client_name: new FormControl('', {
        validators: Validators.required
      }),
      description: new FormControl('', {
        validators: Validators.required
      }),
      start: new FormControl('', {
        validators: Validators.required
      }),
      end: new FormControl('', {

      }),
      seats: new FormControl('', {
        validators: Validators.required
      })
    });

    this.seatService.getSeats()
      .subscribe(seats => {
        console.log(seats);
        this.seats = seats['data'];
      });
  }

  public confirmAdd(formData: any): void {
    const bookingData = this.mapDateData(formData.value);

    this.bookingService.addBooking(bookingData).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }

      console.log(res);
    });
  }


  mapDateData(booking: IBooking): IBooking {
    return booking;
  }

}
