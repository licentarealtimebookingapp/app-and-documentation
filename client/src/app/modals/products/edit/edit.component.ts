import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ProductService} from "../../../product.service";
import {IProduct} from "../../../product";
import {NotificationService} from "../../../services/notification.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  productForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<EditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: IProduct,
              public productService: ProductService, private notificationService: NotificationService) { }


  submit() {

  }

  ngOnInit() {
    this.productForm = new FormGroup ({
      _id: new FormControl(this.data._id, {
      }),
      name: new FormControl(this.data.name, {
        validators: Validators.required
      }),
      price: new FormControl(this.data.price, {
        validators: Validators.required
      }),
      image: new FormControl(this.data.image, {
      })
    });
  }

  public confirmEdit(formData: any): void {
    const productData = this.mapDateData(formData.value);

    this.productService.updateProduct(productData).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }

      console.log(res);
    });
  }


  mapDateData(product: IProduct): IProduct {
    return product;
  }

  onFileChange(event) {
    const reader = new FileReader();

    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        console.log('abc');
        this.data.image = reader.result;
        this.productForm.patchValue({
          image: reader.result
        });
        console.log(this.productForm)
      };
    }
  }

}
