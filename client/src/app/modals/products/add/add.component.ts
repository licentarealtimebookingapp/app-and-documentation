import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IProduct} from "../../../product";
import {ProductService} from "../../../product.service";
import {NotificationService} from "../../../services/notification.service";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  productForm: FormGroup;
  url = '';

  constructor(public dialogRef: MatDialogRef<AddComponent>,
              @Inject(MAT_DIALOG_DATA) public data: IProduct,
              public productService: ProductService, private notificationService: NotificationService) { }


  ngOnInit() {
    this.productForm = new FormGroup ({
      name: new FormControl('', {
        validators: Validators.required
      }),
      price: new FormControl('', {
        validators: Validators.required
      }),
      image: new FormControl('', {
        validators: Validators.required
      })
    });
  }

  public confirmAdd(formData: any): void {
    const productData = this.mapDateData(formData.value);

    this.productService.addProduct(productData).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }
    });
  }

  mapDateData(product: IProduct): IProduct {
    return product;
  }

  onFileChange(event) {
    const reader = new FileReader();

    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.url = reader.result;
        this.productForm.patchValue({
          image: reader.result
        });
      };
    }
  }

}
