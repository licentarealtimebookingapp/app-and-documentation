import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import {NotificationService} from "../../../services/notification.service";
import {ISeat} from "../../../seat";
import {SeatService} from "../../../seat.service";
import {TableService} from "../../../table.service";
import {ITable} from "../../../table";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class SeatAddComponent implements OnInit {

  seatForm: FormGroup;
  tables: ITable[];

  constructor(public dialogRef: MatDialogRef<SeatAddComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ISeat,
              public seatService: SeatService, public tableService: TableService, private notificationService: NotificationService) { }


  ngOnInit() {
    this.seatForm = new FormGroup ({
      name: new FormControl('', {
        validators: Validators.required
      }),
      table_id: new FormControl('', {
        validators: Validators.required
      })
    });

    this.tableService.getTables()
      .subscribe(tables => {
        console.log(tables);
        this.tables = tables['data'];
      });
  }

  public confirmAdd(formData: any): void {
    const seatData = this.mapDateData(formData.value);

    this.seatService.addSeat(seatData).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }

      console.log(res);
    });
  }


  mapDateData(seat: ISeat): ISeat {
    return seat;
  }

}
