import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {NotificationService} from "../../../services/notification.service";
import {SeatService} from "../../../seat.service";

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class SeatDeleteComponent {

  constructor(public dialogRef: MatDialogRef<SeatDeleteComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public seatService: SeatService, private notificationService: NotificationService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.seatService.deleteSeat(this.data.id).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }
    });
  }

}
