import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from "../../../services/notification.service";
import {SeatService} from "../../../seat.service";
import {ISeat} from "../../../seat";
import {TableService} from "../../../table.service";
import {ITable} from "../../../table";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class SeatEditComponent implements OnInit {

  seatForm: FormGroup;
  tables: ITable[];

  constructor(public dialogRef: MatDialogRef<SeatEditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ISeat,
              public seatService: SeatService, private notificationService: NotificationService, public tableService: TableService) { }


  submit() {

  }

  ngOnInit() {
    this.seatForm = new FormGroup ({
      _id: new FormControl(this.data._id, {
      }),
      name: new FormControl(this.data.name, {
        validators: Validators.required
      }),
      table_id: new FormControl(this.data.table_id, {
        validators: Validators.required
      })
    });

    this.tableService.getTables()
      .subscribe(tables => {
        console.log(tables);
        this.tables = tables['data'];
      });
  }

  public confirmEdit(formData: any): void {
    const seatData = this.mapDateData(formData.value);

    this.seatService.updateSeat(seatData).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }

      console.log(res);
    });
  }


  mapDateData(seat: ISeat): ISeat {
    return seat;
  }

}
