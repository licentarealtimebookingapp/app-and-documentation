import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {NotificationService} from "../../../services/notification.service";
import {TableService} from "../../../table.service";

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class TableDeleteComponent {

  constructor(public dialogRef: MatDialogRef<TableDeleteComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public tableService: TableService, private notificationService: NotificationService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.tableService.deleteTable(this.data.id).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }
    });
  }

}
