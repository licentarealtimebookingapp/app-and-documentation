import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import {ITable} from "../../../table";
import {TableService} from "../../../table.service";
import {NotificationService} from "../../../services/notification.service";
import {IUser} from "../../../user";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class TableAddComponent implements OnInit {

  tableForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<TableAddComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ITable,
              public tableService: TableService, private notificationService: NotificationService) { }


  ngOnInit() {
    this.tableForm = new FormGroup ({
      name: new FormControl('', {
        validators: Validators.required
      })
    });
  }

  public confirmAdd(formData: any): void {
    const tableData = this.mapDateData(formData.value);

    this.tableService.addTable(tableData).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }

      console.log(res);
    });
  }


  mapDateData(table: ITable): ITable {
    return table;
  }

}
