import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ITable} from "../../../table";
import {NotificationService} from "../../../services/notification.service";
import {TableService} from "../../../table.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class TableEditComponent implements OnInit {

  tableForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<TableEditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ITable,
              public tableService: TableService, private notificationService: NotificationService) { }


  submit() {

  }

  ngOnInit() {
    this.tableForm = new FormGroup ({
      _id: new FormControl(this.data._id, {
      }),
      name: new FormControl(this.data.name, {
        validators: Validators.required
      })
    });
  }

  public confirmEdit(formData: any): void {
    const tableData = this.mapDateData(formData.value);

    this.tableService.updateTable(tableData).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }

      console.log(res);
    });
  }


  mapDateData(table: ITable): ITable {
    return table;
  }

}
