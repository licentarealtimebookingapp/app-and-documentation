import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import {NotificationService} from "../../../services/notification.service";
import {ISeat} from "../../../seat";
import {SeatService} from "../../../seat.service";
import {IProduct} from "../../../product";
import {IOrder} from "../../../order";
import {OrderService} from "../../../order.service";
import {ProductService} from "../../../product.service";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class OrderAddComponent implements OnInit {

  orderForm: FormGroup;
  seats: ISeat[];
  products: IProduct[];

  constructor(public dialogRef: MatDialogRef<OrderAddComponent>,
              @Inject(MAT_DIALOG_DATA) public data: IOrder,
              public orderService: OrderService, public seatService: SeatService, public productService: ProductService, private notificationService: NotificationService) { }


  ngOnInit() {
    this.orderForm = new FormGroup ({
      name: new FormControl('', {
        validators: Validators.required
      }),
      seats: new FormControl('', {
        validators: Validators.required
      }),
      products: new FormControl('', {
        validators: Validators.required
      }),
      total: new FormControl('', {
      })
    });

    this.productService.getProducts()
      .subscribe(products => {
        console.log(products);
        this.products = products['data'];
      });

    this.seatService.getSeats()
      .subscribe(seats => {
        console.log(seats);
        this.seats = seats['data'];
      });
  }

  public confirmAdd(formData: any): void {
    const orderData = this.mapDateData(formData.value);

    var total = 0;
    var productsArray = this.products;
    orderData.products.forEach(function (value) {
      var found = productsArray.find(o => o._id === value);
      total += found.price;
    });

    orderData.total = total;

    this.orderService.addOrder(orderData).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }

      console.log(res);
    });
  }


  mapDateData(order: IOrder): IOrder {
    return order;
  }

}
