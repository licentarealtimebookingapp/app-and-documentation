import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {NotificationService} from "../../../services/notification.service";
import {OrderService} from "../../../order.service";

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class OrderDeleteComponent {

  constructor(public dialogRef: MatDialogRef<OrderDeleteComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public orderService: OrderService, private notificationService: NotificationService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.orderService.deleteOrder(this.data.id).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }
    });
  }

}
