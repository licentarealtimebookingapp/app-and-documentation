import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from "../../../services/notification.service";
import {SeatService} from "../../../seat.service";
import {ISeat} from "../../../seat";
import {IOrder} from "../../../order";
import {IProduct} from "../../../product";
import {OrderService} from "../../../order.service";
import {ProductService} from "../../../product.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class OrderEditComponent implements OnInit {

  orderForm: FormGroup;
  seats: ISeat[];
  products: IProduct[];

  constructor(public dialogRef: MatDialogRef<OrderEditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: IOrder,
              public seatService: SeatService, private notificationService: NotificationService, public orderService: OrderService, public productService: ProductService) { }


  submit() {

  }

  ngOnInit() {
    this.orderForm = new FormGroup ({
      _id: new FormControl(this.data._id, {
      }),
      name: new FormControl(this.data.name, {
        validators: Validators.required
      }),
      seats: new FormControl(this.data.seats, {
        validators: Validators.required
      }),
      products: new FormControl(this.data.products, {
        validators: Validators.required
      }),
      total: new FormControl(this.data.total, {
      })
    });

    this.seatService.getSeats()
      .subscribe(seats => {
        console.log(seats);
        this.seats = seats['data'];
      });

    this.productService.getProducts()
      .subscribe(products => {
        console.log(products);
        this.products = products['data'];
      });
  }

  public confirmEdit(formData: any): void {
    const orderData = this.mapDateData(formData.value);

    this.orderService.updateOrder(orderData).subscribe(res => {
      if(res['error']) {
        this.notificationService.showError('Try again later.')
      } else {
        this.notificationService.showSuccess(res['message']);
      }

      console.log(res);
    });
  }


  mapDateData(order: IOrder): IOrder {
    return order;
  }

}
