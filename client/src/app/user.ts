export interface IUser {
  id: number;
  name: string;
  description: string;
  email: string;
}
