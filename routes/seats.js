const express = require("express");
const router = express.Router();
const authorizeRequest = require('../middleware/authorize-requests');
const SeatsController = require('../controllers/seats');

router.get("/", authorizeRequest, SeatsController.seats_get_all);

router.post("/", authorizeRequest, SeatsController.seats_create_seat);

router.get("/:seatId", authorizeRequest, SeatsController.seats_get_seat);

router.patch("/:seatId", authorizeRequest, SeatsController.seats_update_seat);

router.delete("/:seatId", authorizeRequest, SeatsController.seats_delete);

module.exports = router;