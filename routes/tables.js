const express = require("express");
const router = express.Router();
const authorizeRequest = require('../middleware/authorize-requests');
const TablesController = require('../controllers/tables');

router.get("/", authorizeRequest, TablesController.tables_get_all);

router.post("/", authorizeRequest, TablesController.tables_create_table);

router.get("/:tableId", authorizeRequest, TablesController.tables_get_table);

router.patch("/:tableId", authorizeRequest, TablesController.tables_update_table);

router.delete("/:tableId", authorizeRequest, TablesController.tables_delete);

module.exports = router;