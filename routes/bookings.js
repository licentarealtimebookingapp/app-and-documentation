const express = require("express");
const router = express.Router();
const authorizeRequest = require('../middleware/authorize-requests');
const BookingsController = require('../controllers/bookings');

router.get("/", authorizeRequest, BookingsController.bookings_get_all);

router.post("/", authorizeRequest, BookingsController.bookings_create_booking);

router.get("/:bookingId", authorizeRequest, BookingsController.bookings_get_booking);

router.patch("/:bookingId", authorizeRequest, BookingsController.bookings_update_booking);

router.delete("/:bookingId", authorizeRequest, BookingsController.bookings_delete);

module.exports = router;