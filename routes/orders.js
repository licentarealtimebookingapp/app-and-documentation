const express = require("express");
const router = express.Router();
const authorizeRequest = require('../middleware/authorize-requests');
const OrdersController = require('../controllers/orders');

router.get("/", authorizeRequest, OrdersController.orders_get_all);

router.post("/", authorizeRequest, OrdersController.orders_create_order);

router.get("/:orderId", authorizeRequest, OrdersController.orders_get_order);

router.get("/:orderId/pdf",/* authorizeRequest,*/ OrdersController.orders_export_order);

router.patch("/:orderId", authorizeRequest, OrdersController.orders_update_order);

router.delete("/:orderId", authorizeRequest, OrdersController.orders_delete);

module.exports = router;