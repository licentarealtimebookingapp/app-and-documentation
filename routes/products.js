const express = require("express");
const router = express.Router();
const multer = require('multer');
const authorizeRequest = require('../middleware/authorize-requests');
const ProductsController = require('../controllers/products');

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});

router.get("/", authorizeRequest, ProductsController.products_get_all);

router.post("/", authorizeRequest, upload.single('image'), ProductsController.products_create_product);

router.get("/:productId", authorizeRequest, ProductsController.products_get_product);

router.patch("/:productId", authorizeRequest, ProductsController.products_update_product);

router.delete("/:productId", authorizeRequest, ProductsController.products_delete);

module.exports = router;