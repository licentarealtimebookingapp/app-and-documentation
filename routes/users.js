const express = require("express");
const router = express.Router();

const authorizeRequest = require('../middleware/authorize-requests');
const UserController = require('../controllers/user');


router.get("/me", authorizeRequest, UserController.me);

router.post("/register", UserController.register);

router.post("/login", UserController.login);

router.patch("/me", authorizeRequest, UserController.update);

module.exports = router;